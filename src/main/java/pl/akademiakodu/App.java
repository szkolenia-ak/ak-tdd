package pl.akademiakodu;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {
        Calculator calculator = new Calculator();
        int add = calculator.add(1, 2);

        assert add == 4;
    }
}
