package pl.akademiakodu;

public class Calculator {

    public int add(int a, int b){
        return a + b;
    }

    public double divide(double a, double b){
        if(b == 0.0){
            throw new IllegalArgumentException("Nie dziel przez zero!");
        }

        return a/b;
    }
}
