package pl.akademiakodu;

public class StringCalculator {

    public int add(String numbers){
        if("".equals(numbers)){
            return 0;
        }

        if(numbers.startsWith("//")){
            String separator = String.valueOf(numbers.charAt(2));
            String cuttedNumbers = numbers.split("\n")[1];

            return splitAndSum(cuttedNumbers, "\\" + separator);

        } else {


            return splitAndSum(numbers, "[,\n]");
        }
    }

    private int splitAndSum(String stringWithNumbers, String separator) {
        String[] splittedNumbers = stringWithNumbers.split(separator);
        int sum = 0;
        for (String number : splittedNumbers) {
            sum += Integer.valueOf(number);
        }
        return sum;
    }
}
