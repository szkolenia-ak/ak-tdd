package pl.akademiakodu;

import org.junit.Assert;
import org.junit.Test;

import static org.junit.Assert.*;

public class StringCalculatorTest {

    StringCalculator stringCalculator = new StringCalculator();

    @Test
    public void shouldReturnZeroWhenEmptyString(){
        int add = stringCalculator.add("");
        assertEquals(0, add);
    }

    @Test
    public void shouldReturnPassedNumberForOneNumber(){
        int add = stringCalculator.add("4");
        Assert.assertEquals(4, add);
        Assert.assertEquals(9, stringCalculator.add("9"));
        Assert.assertEquals(90, stringCalculator.add("90"));
    }

    @Test
    public void shouldReturnSumOfNumbersForTwoNumbers(){
        int add = stringCalculator.add("4,5");
        Assert.assertEquals(9, add);
    }

    @Test
    public void shouldReturnSumOfNumbersForAnyAmountOfNumbers(){
        Assert.assertEquals(10, stringCalculator.add("4,5,1"));
        Assert.assertEquals(20, stringCalculator.add("2,5,1,3,9"));
    }

    @Test
    public void shouldHandleNewLineAsSecondSeparator(){
        Assert.assertEquals(10, stringCalculator.add("4\n5,1"));
    }

    @Test
    public void shouldhandleDynamicSeparator(){
        Assert.assertEquals("Semicolon test",10, stringCalculator.add("//;\n4;5;1"));
        Assert.assertEquals("Pipe test",10, stringCalculator.add("//|\n4|5|1"));
    }
}