package pl.akademiakodu;

import org.hamcrest.CoreMatchers;
import org.junit.Assert;
import org.junit.Rule;
import org.junit.Test;
import org.junit.matchers.JUnitMatchers;
import org.junit.rules.ExpectedException;

import java.util.regex.Matcher;

import static org.junit.Assert.*;

public class CalculatorTest {
    Calculator calculator = new Calculator();

    @Rule
    public ExpectedException expectedException = ExpectedException.none();

    @Test
    public void add() {
        //inicjalizacja
        Calculator calculator = new Calculator();

        //dzialanie
        int add = calculator.add(1, 2);

//        Integer a = 1;
//        Integer b = 1;
//        //Integer.valueOf()
//        Assert.assertSame(a, b);

        //sprawdzenie wynikow
        //assert add == 4;
        //Assert.assertTrue(add == 4);
        assertEquals("Check add",3, add);
        //assertSame(3000, 1000+2000);
    }

    @Test
    public void divideTest(){
        double divide = calculator.divide(1, 2);

        assertEquals(0.5, divide, 1e-15);
    }

    @Test
    public void shouldThrowExceptionWhenDivideByZero(){
        expectedException.expect(IllegalArgumentException.class);
        expectedException.expectMessage(CoreMatchers.endsWith("zero!"));
        double divide = calculator.divide(1.0, 0.0);
        System.out.println(divide);
    }

    @Test
    public void compareObjects(){
        Fruit banana = new Fruit("banana");
        Fruit banana2 = new Fruit("banana");

        assertEquals(banana, banana2);
    }

}