package pl.akademiakodu;

import org.junit.Assert;
import org.junit.Test;
import org.mockito.ArgumentMatchers;
import org.mockito.Mockito;
import org.mockito.internal.verification.VerificationModeFactory;

import java.util.List;

import static org.junit.Assert.assertEquals;
import static org.mockito.ArgumentMatchers.anyInt;
import static org.mockito.ArgumentMatchers.eq;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import static org.mockito.internal.verification.VerificationModeFactory.atLeast;
import static org.mockito.internal.verification.VerificationModeFactory.times;

public class MockingTest {

    @Test
    public void firstMock(){
        Calculator calculator = new Calculator();
        Calculator mockedCalculator = mock(Calculator.class);

        when(mockedCalculator.add(1,2))
                .thenReturn(4)
                .thenReturn(5)
                .thenCallRealMethod();

        System.out.println(mockedCalculator.add(1,2));

        assertEquals(3, calculator.add(1,2));

        assertEquals(5, mockedCalculator.add(1,2));
        assertEquals(3, mockedCalculator.add(1,2));
    }

    @Test
    public void multipleMocks(){
        Calculator mockedCalculator = mock(Calculator.class);
        when(mockedCalculator.add(
                        anyInt(),
                        eq(3)))
                .thenReturn(4)
                .thenReturn(5)
                .thenCallRealMethod();
        when(mockedCalculator.add(
                anyInt(),
                eq(4)))
                .thenReturn(20);

        System.out.println(mockedCalculator.add(1,3));
        System.out.println(mockedCalculator.add(2,4));
        System.out.println(mockedCalculator.add(3,3));
        System.out.println(mockedCalculator.add(3,3));
        System.out.println(mockedCalculator.add(3,3));
    }

    @Test
    public void verifyInvocations(){
        Calculator mockedCalculator = mock(Calculator.class);
        when(mockedCalculator.add(
                anyInt(),
                eq(3)))
                .thenReturn(4)
                .thenReturn(5)
                .thenCallRealMethod();

        System.out.println(mockedCalculator.add(1,3));
        System.out.println(mockedCalculator.add(2,4));
        System.out.println(mockedCalculator.add(3,3));
        System.out.println(mockedCalculator.add(3,3));
        System.out.println(mockedCalculator.add(3,3));

        verify(mockedCalculator, atLeast(3)).add(3, 3);
        verify(mockedCalculator, atLeast(3))
                .add(anyInt(), eq(3));
    }

    @Test
    public void mockingList(){
        //Zmockowana lista, ktora na get od dowolnego parametru zwraca staly string
        //Nalezy wywolac get na liscie dwa razy, z parametrami np. 2, i 5
        //Wynik wyswietlic na ekranie
        List<String> mockedList = Mockito.mock(List.class);
        Mockito.when(mockedList.get(anyInt()))
                .thenReturn("ala")
                .thenReturn("ma", "kota");

        System.out.println(mockedList.get(2));
        System.out.println(mockedList.get(5));
        System.out.println(mockedList.get(10));
        System.out.println(mockedList.get(0));
    }

    @Test
    public void mockingListWithDifferentScenarios(){
        List<String> mockedList = mock(List.class);
        when(mockedList.get(anyInt())).thenReturn("any");
        when(mockedList.get(5)).thenReturn("5");

        System.out.println(mockedList.get(0));
        System.out.println(mockedList.get(10));
        System.out.println(mockedList.get(5));
        System.out.println(mockedList.get(7));

        verify(mockedList, VerificationModeFactory.times(1))
                .get(5);

        verify(mockedList, times(2))
                .get(ArgumentMatchers.intThat(argument -> argument > 5));
    }
}
